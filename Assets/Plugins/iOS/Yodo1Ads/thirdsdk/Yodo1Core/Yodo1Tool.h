//
//  Yodo1Tool.h
//  Yodo1sdk
//
//  Created by hyx on 2020/2/3.
//

#import <Foundation/Foundation.h>
#import "Yodo1Base.h"

NS_ASSUME_NONNULL_BEGIN

#define Yd1OpsTools  Yodo1Tool.shared

@interface Yodo1Tool : Yodo1Object

+ (instancetype)shared;

@end

NS_ASSUME_NONNULL_END
