//
//  Yodo1ShareUI.h
//  Yodo1sdk
//
//  Created by hyx on 2019/11/28.
//

#import <Foundation/Foundation.h>
#import "Yodo1Base.h"

NS_ASSUME_NONNULL_BEGIN

@interface Yodo1UI : Yodo1Object

+ (instancetype)shared;

@end

NS_ASSUME_NONNULL_END
