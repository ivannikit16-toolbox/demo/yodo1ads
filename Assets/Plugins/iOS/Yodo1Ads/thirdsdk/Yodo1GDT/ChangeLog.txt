=============================================
2020-05-25
4.11.8	2020-05-08	支持开屏视频，修复已知问题

=============================================
2020-04-22
4.11.7	2020-03-30	修复开屏预加载接口问题
=============================================
2020-2-03
4.11.6	2020-03-30	
插屏全屏视频支持Endcard和底部卡片、修复已知问题


=============================================
2020-2-03
4.11.3	2020-01-14	修复bug


=============================================
2019-12-31
4.11.2	2019-12-19	修复bug
4.11.1	2019-12-3	修复bug
4.11.0	2019-11-25	修复bug
4.10.19	2019-10-29	支持插屏视频，支持设置渠道号，修复bug

=============================================
2019-11-20
1.广点通sdk v4.10.19 iOS13兼容性问题修复

=============================================
2019-10-14
1.广点通sdk v4.10.14 iOS13兼容性问题修复

=============================================
2019-09-23
1.广点通sdk v4.10.11 iOS13兼容性问题修复

=============================================
2019-06-03
1.广点通sdk v4.10.2 [修复被苹果拒审的BUG]

=============================================
2019-05-23
1.广点通sdk v4.10.1
iOS 4.10.1，支持Banner广告，插屏广告，原生广告，
开屏广告，原生模板广告，激励视频广告，自渲染2.0广告，
Banner2.0广告，插屏2.0广告，H5激励视频广告，
优化直达广告，修复bug

=============================================
2019-05-05
1.广点通sdk v4.8.10

=============================================
2019-04-08
1.广点通sdk v4.8.7

=============================================
2018-11-08
1.广点通sdk v4.8.1

=============================================
2018-09-18
1.广点通sdk v4.8.0

=============================================
2018-07-24
1.广点通sdk v4.7.7

=============================================
2018-05-04
1.广点通sdk v4.7.4

=============================================
2018-04-24
1.广点通sdk v4.7.3

=============================================
2018-04-02
1.广点通sdk v4.7.1
支持Banner广告，插屏广告，原生广告，开屏广告，原生模板广告。优化开屏广告效果。
=============================================
2018-01-11
1.广点通sdk v4.7.0
支持Banner广告，插屏广告，原生广告，
开屏广告，原生模板广告。适配 iPhone X、
支持 Auto Layout。

=============================================
2017-10-11
1.广点通sdk v4.6.2
